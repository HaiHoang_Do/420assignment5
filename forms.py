from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, Length


class AddressForm(FlaskForm):
    name = StringField('name', validators=[DataRequired(), Length(min=1, max=50)])
    street = StringField('street', validators=[DataRequired(), Length(min=1, max=100)])
    city = StringField('city', validators=[DataRequired(), Length(min=1, max=50)])
    province = StringField('province', validators=[DataRequired(), Length(min=1, max=2)])

    submit = SubmitField('Add')