--Drops Tables
DROP TABLE Note;
DROP TABLE Address_Book;

--Creates Tables
CREATE TABLE Address_Book(
    author_id   NUMBER(4)   GENERATED ALWAYS AS IDENTITY(START WITH 1001 INCREMENT BY 1) PRIMARY KEY,
    user_name   VARCHAR2(50),
    street      VARCHAR2(100),
    city        VARCHAR2(50),
    province    VARCHAR2(2)
);

CREATE TABLE Note(
    note_id     NUMBER(4)   GENERATED ALWAYS AS IDENTITY(START WITH 1000 INCREMENT BY 1) PRIMARY KEY,
    author_id   NUMBER(4)   REFERENCES Address_Book(author_id),
    content     VARCHAR2(140)
);

--Inserts Data
INSERT INTO Address_Book(user_name, street, city, province)
VALUES('Sara', 'street 1', 'LAVAL', 'QC');
INSERT INTO Address_Book(user_name, street, city, province)
VALUES('Mays', 'street 2', 'Toronto', 'ON');
INSERT INTO Address_Book(user_name, street, city, province)
VALUES('Jamil', 'street 3', 'MONTREAL', 'QC');

INSERT INTO Note(author_id, content)
VALUES(1001, 'Do you know who am I?');
INSERT INTO Note(author_id, content)
VALUES(1001, 'I go to college');
INSERT INTO Note(author_id, content)
VALUES(1002, 'I was very young');
INSERT INTO Note(author_id, content)
VALUES(1003, 'Life is too too short');
INSERT INTO Note(author_id, content)
VALUES(1003, 'Need all kinds of strengths to fully succeed!');

COMMIT;