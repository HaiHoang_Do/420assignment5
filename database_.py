import os
import oracledb
import config_db

class Database:
    def __init__(self, autocommit=True):
        self.__connection = self.__connect()
        self.__connection.autocommit = autocommit

    def __connect(self):
        the_user = config_db.usr
        pw = config_db.pw
        the_host = config_db.host
        sn = config_db.sn
        return oracledb.connect(host=the_host, user=the_user, password=pw, service_name=sn) 
    
    def  db_conn (self): 
        return self.__connection

    def __run_file(self, file_path):
        statement_parts = []
        with self.__connection.cursor() as cursor:
            # pdb.set_trace()
            with open(file_path, 'r') as f:
                for line in f:
                    if line[:2]=='--': continue
                    statement_parts.append(line)
                    if line.strip('\n').strip('\n\r').strip().endswith(';'):
                        statement = "".join( statement_parts).strip().rstrip(';')
                        if statement:
                            try:
                                # pdb.set_trace()
                                cursor.execute(statement)
                            except Exception as e:
                                print(e)
                        statement_parts = []

    
    def add_address(self, address):
        '''Add an address to the DB for the given Address object'''
        pass

    def get_address(self, name):
        '''Returns an Address object based on the provided name'''
        pass

    # def get_addresses(self):
    #     '''Returns all Address objects in a list'''
    #     pass

    def close(self):
        '''Closes the connection'''
        if self.__connection is not None:
            self.__connection.close()
            self.__connection = None

    def get_cursor(self):
            for i in range(3):
                try:
                    return self.__connection.cursor()
                except Exception as e:
                    # Might need to reconnect
                    self.__reconnect()

    def __reconnect(self):
        try:
            self.close()
        except oracledb.Error as f:
            pass
        self.__connection = self.__connect()

    

    def run_sql_script(self, sql_filename):
        if os.path.exists(sql_filename):
            self.__connect()
            self.__run_file(sql_filename)
            self.close()
        else:
            print('Invalid Path')
# ----------------------------------
# ------- DML DB Operations  
    def get_addresses(self, cond):
        # method to list all (if cond is True) or some address books based on cond
        try:
            with self.db_conn().cursor() as cur:
                if cond == True:
                    cur.execute("SELECT * FROM Address_Book")
                else:
                    cur.execute("SELECT * FROM Address_Book WHERE Username=:cond", (cond,))
                result = cur.fetchall()
                return result

        except oracledb.Error as err:
            raise err
        
# ----------------------
    def get_address_field(self, field, cond):
            #  method to get the column field based on condition
        pass 
      
# ----------------------                   
    def get_notes_user(self, username):
            # method to list all notes of a given user
        try:
            with self.__connection.cursor() as cur:
                cur.execute("SELECT * FROM Note WHERE username=:username", (username,))

                note_list = cur.fetchall()
                return note_list
        
        except oracledb.Error as err:
            raise err
# ---------------------
    def get_owner(self, username):
        try:
            with self.__connection.cursor() as cur:
                cur.execute("SELECT * FROM Owner WHERE username=:usename", (username,))

                owner = cur.fetchone()
                return owner
        
        except oracledb.Error as err:
            raise err

# ----------------------
    def add_new_owner(self, username, owner_name, email, password, occupation):
            try:
                with self.__connection.cursor() as cur:
                    active = 1
                    owner_image = 'default-user.jpg'
                    cur.execute('INSERT INTO Owner (active , username,  owner_name, email , password, occupation, owner_image) VALUES(:active, :username, :owner_name, :email, :password, :occupation, :owner_image)', (active, username, owner_name, email, password, occupation, owner_image))
                    self.__connection.commit()

            except oracledb.Error as err:
                raise err  

# ----------------------
    def add_new_address(self, user_name, street, city, province):
            #  method to add a new address, data coming fro a user input form
        try:
            with self.__connection.cursor() as cur:
                if self.get_owner(user_name) == None:
                    self.add_new_owner(user_name, user_name, "none", "none", "none")
                cur.execute('INSERT INTO Address_Book(username, street, city, province) VALUES(:user_name, :street, :city, :province)', (user_name, street, city, province))
                self.__connection.commit()

        except oracledb.Error as err:
            raise err
           

                
# ----------------------                   
# ----------------------------
#  1.	Create two global variables:
# •	an instance of Database class that you call db.
db=Database()

# 2.	Call run_sql_script on database.sql  if the script (database.py) is run in isolation.
if __name__ == '__main__':
    db.run_sql_script('./schema.sql')
