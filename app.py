from flask import Flask, render_template, url_for, abort, redirect, flash
from address import Address
from note import Note
from forms import AddressForm
from database_ import db

app = Flask(__name__)
app.config['SECRET_KEY'] = '1234'

#Creates global variables
addresses = []
notes = [Note(1, "This is note 1"), Note(2, "This is note 2"), Note(3, "This is note 3")]

#Function to refresh the global list of addresses by clearing and appending to the list
def refresh_addresses():
    global addresses
    addresses.clear()
    try:
        for row in db.get_addresses(True):
            addresses.append(Address(row[0], row[1], row[2], row[3], row[4]))
    except:
        addresses = ['Error retrieving from database']

refresh_addresses()


@app.errorhandler(404)
def page_not_found(e):
    return render_template('custom404.html'), 404

@app.route("/")
def home():
    return render_template('home.html')

@app.route("/addressbook/<string:name>")
def address_book_search(name):
    #If the parameter 'name' is empty, redirects the user back to the addresses page
    if name == "":
        return redirect(url_for('address_book'))
    #Searches through list of existing addresses
    #If the parameter matches one of the addresses in the list, creates an Address object
    #The object id is used to retrieve the list of notes associated to this id
    for a in addresses:
        if a.name == name:
            user_address = Address(db.get_addresses(name)[0][0], db.get_addresses(name)[0][1], db.get_addresses(name)[0][2], db.get_addresses(name)[0][3], db.get_addresses(name)[0][4])

            user_notes = []
            for row in db.get_notes_user(name):
                user_notes.append(Note(row[0], row[2]))
            return render_template('specific_address.html', address=user_address, notes=user_notes)
    
    #Flashes error message if parameter does not match any name in the list of existing addresses
    flash("Address can not be found", 'error')
    return redirect(url_for('address_book'))
            
@app.route("/addressbook", methods=['GET', 'POST'])
def address_book():
    #Refreshed the list of existing addresses everytime this page is loaded
    refresh_addresses()

    #Creates form object to be used in this page
    address_form = AddressForm()
    if address_form.validate_on_submit():
        #Calls function to insert a new address into database using the form inputs
        #Flashes a status message depending on the result of running the query
        try:
            db.add_new_address(address_form.name.data, address_form.street.data, address_form.city.data, address_form.province.data)
            flash('Address added', 'success')
        except Exception as e:
            # flash('Error adding address', 'error')
            flash(e, 'error')

        return redirect(url_for('address_book'))

    return render_template('address.html', addresses=addresses, form=address_form)

@app.route("/notes/<int:id>")
def note_search(id):
    #If parameter 'id' is empty, redirects user back to the notes page
    if id == "":
        return redirect(url_for('note_book'))
    
    #IF parameter matches an entry in the existing list of notes, the note's content is returned
    for n in notes:
        if n.id == id:
            return n.str()
    
    return redirect(url_for('note_book'))            
            
@app.route("/notes")
def note_book():
    #Shows all entries in the existing list of notes using and unordered list
    list = "<ul>"
    for n in notes:
        list += f"<li><a href='{url_for('note_search', id=n.id)}'>Note {n.id}</a></li>"
        
    list += "</ul>"
    
    return list

if __name__ == '__main__':
    db.run_sql_script('schema.sql')
    app.run()